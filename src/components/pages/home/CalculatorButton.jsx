import React, { Component } from 'react';

// const CalculatorButton = (props) => {

//     return <div className="operator-button">
//          <button name="tambah" className="btn btn-success">
//              <h6>{props.button}</h6>
//          </button>
//     </div>
// }

const CALCULATOR_MAPPER = {
    "+": "btn btn-success",
    "-": "btn btn-danger",
    "*": "btn btn-info",
    "/": "btn btn-warning"
}
    
const CalculatorButton = ({operator, onOperatorChange}) => (
    <button style={{margin:"0.2em"}}
        onClick={() => onOperatorChange(operator)}
        className={CALCULATOR_MAPPER[operator]}>{operator}</button>
);
export default CalculatorButton;