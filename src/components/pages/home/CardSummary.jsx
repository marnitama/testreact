import React from "react"
import './css/card-summary.css'
import '../../../../node_modules/font-awesome/css/font-awesome.min.css'


const CardSummary = (props) => 
<div className="card-summary-block" style={{backgroundColor: props.card.backgroundColor}}>
    <div className="card-summary-content">
        <div className="card-summary-value">
            <h2>{props.card.value}</h2>
            <h6>{props.card.name}</h6>
        </div>
        <div className="card-summary-icon"> 
            <i class={props.card.icon} aria-hidden="true"></i>
            

        </div>
    </div>
    <div className="card-summary-footer">
        <h6>More Info <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h6>
        
    </div>
</div>

export default CardSummary;