import React, { Component } from 'react';
import CalculatorButton from './CalculatorButton';

const  button= ["+","-","*","/"]

const CalculatorContainer = ({onOperatorChange}) => 
    button.map((operator) => 
        <CalculatorButton onOperatorChange={onOperatorChange} operator={operator}/>
    )

export default CalculatorContainer;