import React, { Component } from 'react';
import CardSummary from './CardSummary'
import './css/card-summary.css'

class CardSummaryContainer extends Component {
    state = {
        summary: [
            {
                name: "New Orders",
                value: 150,
                icon: "fa fa-shopping-bag fa-4x",
                backgroundColor: "#37c7fc"
            },
            {
                name: "Bounce Rate",
                value: 50 + "%",
                icon: "fa fa-bar-chart fa-4x",
                backgroundColor: "#3d9951"
            },
            {
                name: "User Registration",
                value: 44,
                icon: "fa fa-user-plus fa-4x",
                backgroundColor: "#ff7f00"
            },
            {
                name: "Unique Visitors ",
                value: 65,
                icon: "fa fa-pie-chart fa-4x",
                backgroundColor:"#db4f48"
            },

        ]
      }

      

    renderCardSummary = () => {
    const {summary} = this.state;
    return summary.map((card) => (
       <CardSummary card={card} />
       ))
    }

    render() {
        const {summary} = this.state;
        return <div className = "card-summary-parent">
            {this.renderCardSummary(summary)}
        </div>
    }
}

export default CardSummaryContainer