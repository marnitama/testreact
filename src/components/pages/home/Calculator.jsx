import React, { Component } from 'react';
import { switchCase } from '@babel/types';
import './css/calculator.css'
import CalculatorButton from './CalculatorButton';
import CalculatorContainer from './CalculatorContainer'

class Calculator extends Component {
    state = {
        number: {
            firstNumber: 0,
            secondNumber: 0,
        },
       

        operator: "+",
    };

    handleChange = event => {
        const { value, name } = event.target;
        let {number} = this.state;
        number[name] = parseInt(value);
        this.setState({ number })

    }

    handleOperatorChange = value => {
        this.setState({ operator: value })
    }

   getResult = (firstNumber, operator, secondNumber) => {
        switch (operator){
            case "+":
                return firstNumber + secondNumber
            case "-":
                return firstNumber - secondNumber
            case "*":
                return firstNumber * secondNumber
            default:
                return firstNumber / secondNumber
        }
    }

    renderCalculatorButton = () => {
        const {currOperators} = this.state;
        return currOperators.map((button) => (
            <CalculatorButton key={button} button={button} />
        ))
    }

    render() { 
        const {firstNumber, secondNumber} = this.state.number;
        const {operator} = this.state;
        return ( 
        <div className = "calculator-container">
            <h1>Kalkulator Sederhana</h1>
            
            <div className = "calculator-input">
                <input name="firstNumber" onChange = {this.handleChange} defaultValue={firstNumber}/>
                {operator}
                <input name="secondNumber" onChange = {this.handleChange} defaultValue={secondNumber}/>
                =
                <input value={this.getResult(firstNumber,operator,secondNumber)} disabled/> 
            </div>
            <div className="calculator-button">
                <CalculatorContainer onOperatorChange = {this.handleOperatorChange}/>
            </div>


            {/* <div className = "calculator-button">
                <button onClick={(e) => this.handleOperatorChange(e)} 
                    value="+" name="tambah" className="btn btn-success">+</button>
                <button onClick={(e) => this.handleOperatorChange(e)} 
                value="-" name="kurang" className="btn btn-warning">-</button>
                <button onClick={(e) => this.handleOperatorChange(e)} 
                value="*" name="kali" className="btn btn-info">*</button>
                <button onClick={(e) => this.handleOperatorChange(e)} 
                value="/" name="bagi" className="btn btn-danger">/</button>
            </div> */}
        </div> 
        );
    }

    

   
}
 
export default Calculator;

