import React, { Component } from 'react';
import CardSummaryContainer from '../components/pages/home/CardSummaryContainer.jsx';
import Calculator from '../components/pages/home/Calculator.jsx';

class Home extends Component {
    state = {  }
    render() { 
        return (<div>
            <h1 style={{textAlign:"center", marginBottom:"1em"}}>Dashboard</h1>
            <CardSummaryContainer/>
            <br></br> 
            <Calculator/>
            </div>);
    }
}
 
export default Home;